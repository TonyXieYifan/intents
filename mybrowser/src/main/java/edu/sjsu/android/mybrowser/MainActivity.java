package edu.sjsu.android.mybrowser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String website = getIntent().getDataString();
        TextView text = findViewById(R.id.website);
        text.setText(website);
    }

    public void getBack(View view){
        // Set the activity's result to RESULT_OK
        setResult(RESULT_OK);
        // Finish and close the current activity
        finish();
    }
}