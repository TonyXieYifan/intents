package edu.sjsu.android.exercise3tonyxie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    public static final int TEXT_REQUEST = 1;
    public static final int VIEW_REQUEST = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void choose(View view) {

        // If the explicit button ("Set Text") is clicked
        if (view.getId() == R.id.explicit) {

            // Create an explicit Intent with a specific activity (TextActivity)
            Intent explicit = new Intent(this, TextActivity.class);

            // Start an explicit intent for result
            // with request code for text request
            startActivityForResult(explicit, TEXT_REQUEST);
        }

        // If the implicit button ("Web Browser") is clicked
        else if (view.getId() == R.id.implicit) {

            // Let the data to be amazon's homepage
            Uri website = Uri.parse("http://www.amazon.com");

            // Create an implicit Intent with a specific action (view) and the data
            Intent implicit = new Intent(Intent.ACTION_VIEW, website);

            // Create a chooser for the implicit Intent
            // so the user can choose the app to perform the action
            Intent choose = Intent.createChooser(implicit,
                    "Load Amazon.com with:");

            // Start the chooser for result with request code for view request
            startActivityForResult(choose, VIEW_REQUEST);
            //startActivityForResult(implicit, VIEW_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        String result = "";

        // You can also use if-else-if statements here
        switch (requestCode) {
            case TEXT_REQUEST:// For the text request

                if (resultCode == RESULT_OK) // If user confirmed

                    // Get data from the intent got from TextActivity
                    // EXTRA_TEXT is The key of the extra,
                    // a public static final string defined in TextActivity
                    result = data.getStringExtra(TextActivity.EXTRA_TEXT);

                else if (resultCode == RESULT_CANCELED) // If user cancelled
                    result = "Cancelled";

                break;

             case VIEW_REQUEST: // For the view request

                 if (resultCode == RESULT_OK)
                     result = "Website visited";

                 break;
        }

        // Set the text to the corresponding result
        TextView text = findViewById(R.id.result);
        text.setText(result);
    }
}