package edu.sjsu.android.exercise3tonyxie;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class TextActivity extends AppCompatActivity {

    // Set unique key for extra
    public final static String EXTRA_TEXT ="edu.sjsu.android.text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);
    }

    public void setText(View view){
        Intent replyIntent = new Intent();
        if(view.getId() == R.id.confirm){
            // If confirm button is clicked, set the data as your name
            replyIntent.putExtra(EXTRA_TEXT, "Tony Xie");
            // And set the activity's result to RESULT_OK
            setResult(RESULT_OK, replyIntent);
        } else if (view.getId() == R.id.cancel){
            // If cancel button is clicked, set result to RESULT_CANCEL
            setResult(RESULT_CANCELED, replyIntent);
        }
        // Finish and close the current activity
        finish();
    }
}